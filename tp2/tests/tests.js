QUnit.test( "isEven", function( assert ) {
  assert.expect( 6 );
  assert.ok(isEven(2));
  assert.notOk(isEven(3));
  assert.notOk(isEven(5));
  assert.notOk(isEven(17));
  assert.ok(isEven(20));
  assert.ok(isEven(0));
});

QUnit.test( "bonjour", function( assert ) {
  assert.expect( 6 );
  assert.equal(bonjour("Sophie"), "Bonjour Sophie");
  assert.equal(bonjour("l'ami"), "Bonjour l'ami");
  assert.equal(bonjour("Monsieur"), "Bonjour Monsieur");
  assert.equal(bonjour("Paul"), "Bonjour Paul");
  assert.equal(bonjour("Georges"), "Bonjour Georges");
  assert.equal(bonjour("bonjour !"), "Bonjour bonjour !");
});

QUnit.test( "Sécu", function( assert ) {
  //assert.expect( 3 );
  //assert.notOk(isSecuCorrect("1-95-11-41-018-242-68"));
  assert.ok(isSecuCorrect("1 95 11 41 018 255 65"));
  assert.notOk(isSecuCorrect("10 95 11 41 018 255 65"));
});


QUnit.test( "RIB", function( assert ) {
  assert.expect( 3 );
  assert.notOk(isRib(11111, 22222, "ABCD3333EFG", 42));
  assert.ok(isRib(30002, 00550, 0000157845, 89));
  assert.notOk(isRib(30002, 00110, 0000153845, 02));
});