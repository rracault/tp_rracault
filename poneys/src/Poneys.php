<?php 
  class Poneys {
      private $count = 8;

      public function getCount() {
        return $this->count;
      }

      public function setCount($number) {
        $this->count = $number;
      }

      public function removePoneyFromField($number) {
        if ($this->count - $number < 0) {
          throw new Exception("NegativeCountException");
          
        }
        $this->count -= $number;
      }

      public function getNames() {
        //TODO: implementation
      }

      public function addPoneyToField($number) {
        $this->count += $number;
      }

      public function placeAvailable() {
        return ($this->count < 15);
      }
  }
?>
