<?php
  use PHPUnit\Framework\TestCase;
  require_once 'src/Poneys.php';

  class PoneysTest2 extends TestCase {
    

    public function setCountProvider() {
      return [
        [5],
        [8],
        [2],
        [0],
        [15]
      ];
    }

    /**
     * @dataProvider setCountProvider
     */
    public function test_setCount($number) {
      //Setup
      $Poneys = new Poneys();
      // Action
      $Poneys->setCount($number);
      // Assert
      $this->assertEquals($number, $Poneys->getCount());
    }


  }

?>
