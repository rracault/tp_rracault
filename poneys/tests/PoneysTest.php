<?php
  use PHPUnit\Framework\TestCase;
  require_once 'src/Poneys.php';

  class PoneysTest extends TestCase {
    

    private $Poneys;

    protected function setUp() {
      $this->Poneys = new Poneys();
    }

    protected function tearDown() {
      $this->Poneys = null;
    }

    public function removePoneyFromFieldProvider() {
      return [
        [5],
        [8],
        [2],
        [0],
      ];
    }

    /**
     * @dataProvider removePoneyFromFieldProvider
     */
    public function test_removePoneyFromField($number) {
      // Action
      $this->Poneys->removePoneyFromField($number);
      // Assert
      $this->assertEquals(8 - $number, $this->Poneys->getCount());
    }

    public function test_addPoneyToField() {
      // Action
      $this->Poneys->addPoneyToField(2);
      // Assert
      $this->assertEquals(10, $this->Poneys->getCount());
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage NegativeCountException
     */
    public function test_removePoneyFromFieldException() {
      // Assert
      $this->Poneys->removePoneyFromField(9);
    }

/*
    public function test_getNames() {
      $poneyNames = $this->getMock('');
    }
*/

    public function test_getNames() {
      $poneysNamesMock = $this->getMockBuilder('Poneys')->getMock();
      $poneysNamesMock
        ->method('getNames')
        ->willReturn( ['a', 'b', 'c'] );

      $this->assertEquals(['a', 'b', 'c'], $poneysNamesMock->getNames());
    }

    public function placeAvailableProvider() {
      return [
        [5, 5],
        [8, 15],
        [2, 9],
        [0, 2],
        [7, 3],
      ];
    }

    /**
     * @dataProvider placeAvailableProvider
     */
    public function test_placeAvailable($rem, $add) {
      // Action
      $this->Poneys->removePoneyFromField($rem);
      $this->Poneys->addPoneyToField($add);
      $bool;
      if (8 - $rem + $add < 15) {
        $bool = true;
      }
      else
      {
        $bool = false;
      }
      // Assert
      $this->assertEquals($bool, $this->Poneys->placeAvailable());
    }

    public function createPoneyProvider() {
      return [
        [5],
        [8],
        [15],
        [0],
        [7],
      ];
    }

    /**
     * @dataProvider createPoneyProvider
     */
    public function test_createPoney($number) {
      // Action
      $this->Poneys->setCount($number);
      // Assert
      $this->assertEquals($number, $this->Poneys->getCount());
    }


  }

?>
